//
//  Player.m
//  SmashingHead
//
//  Created by Guillermo Zafra on 16/01/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "Player.h"

@implementation Player

-(instancetype)init{
    if (self = [super initWithColor:[UIColor greenColor] size:ccs(40, 150)]) {
        // Init the head
        self.head = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:ccs(100, 200)];
        
        SKShapeNode *head = [SKShapeNode node];
        UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(- self.head.frame.size.width * 0.7f, cgminy(self.head.frame), 100, 200)];
        head.path = path.CGPath;
        head.fillColor = [UIColor greenColor];
        head.strokeColor = [UIColor greenColor];
        head.antialiased = YES;
        head.position = ccp(cgmidx(self.head.frame), cgmaxy(self.head.frame));
        [self.head addChild:head];
        [self.head setAnchorPoint:ccp(0.7f, 0)];
        [self.head setPosition:ccp(cgmaxx(self.frame), cgmaxy(self.frame)-40)];
        [self addChild:self.head];
        
    }
    return self;
}

-(void)hit{
    float speed = ((DEGREES_TO_RADIANS(90.0f) - fabs(self.head.zRotation)) / DEGREES_TO_RADIANS(90.0f)) * 0.2f;
    [self.head removeAllActions];
    [self.head runAction:[SKAction sequence:@[[SKAction rotateToAngle:DEGREES_TO_RADIANS(-90) duration:speed],
                                              [SKAction rotateToAngle:DEGREES_TO_RADIANS(0) duration:1.0f]]]];
    
}

@end
