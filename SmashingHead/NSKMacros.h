//
//  NSKMacros.h
//  Blackjack
//
//  Created by Guillermo Zafra on 16/12/2013.
//  Copyright (c) 2013 Nektan. All rights reserved.
//

#ifndef Blackjack_NSKMacros_h
#define Blackjack_NSKMacros_h

/*! Macros for cocos2d shortcuts */
#define ccp(__X__,__Y__) CGPointMake((float) __X__, (float)__Y__)
#define ccs(__W__, __H__) CGSizeMake((float)__W__, (float)__H__)

/*! Laziness at its finest */
#define cgmidx(__FRAME__) CGRectGetMidX(__FRAME__)
#define cgmidy(__FRAME__) CGRectGetMidY(__FRAME__)
#define cgminx(__FRAME__) CGRectGetMinX(__FRAME__)
#define cgminy(__FRAME__) CGRectGetMinY(__FRAME__)
#define cgmaxx(__FRAME__) CGRectGetMaxX(__FRAME__)
#define cgmaxy(__FRAME__) CGRectGetMaxY(__FRAME__)

#define ccpMult(__POINT__, __MULTIPLIER__) CGPointMake((float) __POINT__.x * __MULTIPLIER__, (float)__POINT__.y * __MULTIPLIER__)
#define ccpMultSize(__POINT1__, __SIZE__) CGPointMake((float) __POINT1__.x * __SIZE__.width, (float)__POINT1__.y * __SIZE__.height)
#define ccpAdd(__POINT1__, __POINT2__) ccp(__POINT1__.x + __POINT2__.x, __POINT1__.y + __POINT2__.y)
#define ccpMidPoint(__POINT1__, __POINT2__) ccpMult(ccpAdd(__POINT1__, __POINT2__), 0.5f)
#define ccpSub(__POINT1__, __POINT2__) ccp(__POINT1__.x - __POINT2__.x, __POINT1__.y - __POINT2__.y)
#define ccpPerp(__POINT__) ccp(-__POINT__.y, __POINT__.x)

/** @def CONVERT_TO_LEFTBOTTOM_ORIGIN
 Converts a position within a node as if it was around left bottom origin
 (Easier to position elements than around center origin)
 */
#define CONVERT_TO_LEFTBOTTOM_ORIGIN(__POSITION, __FRAME) CGPointMake(__POSITION.x * __FRAME.size.width - __FRAME.size.width / 2, __POSITION.y * __FRAME.size.height - __FRAME.size.height / 2)

/** @def DEGREES_TO_RADIANS
 converts degrees to radians
 */
#define DEGREES_TO_RADIANS(__ANGLE__) ((__ANGLE__) * 0.01745329252f) // PI / 180

/** @def RADIANS_TO_DEGREES
 converts radians to degrees
 */
#define RADIANS_TO_DEGREES(__ANGLE__) ((__ANGLE__) * 57.29577951f) // PI * 180

/** @def StringFromBOOL
 converts a boolean value to string ("true" or "false")
 */
#define StringFromBOOL(aBOOL)    aBOOL? @"true" : @"false"

#endif

