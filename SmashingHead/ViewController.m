//
//  ViewController.m
//  SmashingHead
//
//  Created by Guillermo Zafra on 16/01/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "ViewController.h"
#import "MyScene.h"

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    // Configure the view.
    SKView * skView = (SKView *)self.view;
    if (!skView.scene) {
        skView.showsFPS = YES;
        skView.showsNodeCount = YES;
        
        // Create and configure the scene.
        SKScene * scene;
        
        if (skView.bounds.size.height/skView.bounds.size.width < 1.70){
            scene = [MyScene sceneWithSize:CGSizeMake(960, 640)];
        }else{
            
            scene = [MyScene sceneWithSize:CGSizeMake(1136, 640)];
        }
        //        scene.scaleMode = SKSceneScaleModeResizeFill;
        
        // Present the scene.
        [skView presentScene:scene];
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

@end
