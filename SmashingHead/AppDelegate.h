//
//  AppDelegate.h
//  SmashingHead
//
//  Created by Guillermo Zafra on 16/01/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
