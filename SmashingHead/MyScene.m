//
//  MyScene.m
//  SmashingHead
//
//  Created by Guillermo Zafra on 16/01/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "MyScene.h"
#import "Player.h"
#import <CoreMotion/CoreMotion.h>
#import "GPUImage.h"

@interface MyScene()

@property (nonatomic, strong) Player* player;
@property (nonatomic, strong) CMMotionManager *motionManager;

@end

@implementation MyScene{
    
    GPUImageVideoCamera *videoCamera;
    GPUImageMotionDetector *motionDetector;
    
    SKSpriteNode *firstBg;
    SKSpriteNode *secondBg;
    
    float speed;
}

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        
        self.backgroundColor = [SKColor colorWithRed:0.15 green:0.15 blue:0.3 alpha:1.0];
        
        
        self.player = [[Player alloc] init];
        self.player.position = ccp(50, 20);
        self.player.zPosition = 100;
        [self addChild:self.player];
        
        // Bg
        firstBg = [SKSpriteNode spriteNodeWithImageNamed:@"bg.jpg"];
        firstBg.anchorPoint = ccp(0, 0);
        firstBg.position = ccp(cgminx(self.frame), cgminy(self.frame));
        secondBg = [firstBg copy];
        secondBg.position = ccp(firstBg.frame.size.width, cgminy(self.frame));
        [self addChild:firstBg];
        [self addChild:secondBg];
        
        
        
        self.motionManager = [[CMMotionManager alloc] init];
        self.motionManager.accelerometerUpdateInterval = .05;
        
        
        [self.motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue]
                                                 withHandler:^(CMAccelerometerData  *accelerometerData, NSError *error) {
                                                     [self outputAccelertionData:accelerometerData.acceleration];
                                                     if(error){
                                                         
                                                         NSLog(@"%@", error);
                                                     }
                                                 }];
        
        videoCamera = [[GPUImageVideoCamera alloc] initWithSessionPreset:AVCaptureSessionPreset640x480 cameraPosition:AVCaptureDevicePositionFront];
        //    videoCamera = [[GPUImageVideoCamera alloc] initWithSessionPreset:AVCaptureSessionPreset1280x720 cameraPosition:AVCaptureDevicePositionBack];
        //    videoCamera = [[GPUImageVideoCamera alloc] initWithSessionPreset:AVCaptureSessionPreset1920x1080 cameraPosition:AVCaptureDevicePositionBack];
        
        videoCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
        videoCamera.horizontallyMirrorFrontFacingCamera = NO;
        videoCamera.horizontallyMirrorRearFacingCamera = NO;
        
        motionDetector = [[GPUImageMotionDetector alloc] init];
        
        //    filter = [[GPUImageTiltShiftFilter alloc] init];
        //    [(GPUImageTiltShiftFilter *)filter setTopFocusLevel:0.65];
        //    [(GPUImageTiltShiftFilter *)filter setBottomFocusLevel:0.85];
        //    [(GPUImageTiltShiftFilter *)filter setBlurSize:1.5];
        //    [(GPUImageTiltShiftFilter *)filter setFocusFallOffRate:0.2];
        
        //    filter = [[GPUImageSketchFilter alloc] init];
        //    filter = [[GPUImageSmoothToonFilter alloc] init];
        //    GPUImageRotationFilter *rotationFilter = [[GPUImageRotationFilter alloc] initWithRotation:kGPUImageRotateRightFlipVertical];
        
        [videoCamera addTarget:motionDetector];
        __unsafe_unretained typeof(self) weakSelf = self;
        [motionDetector setMotionDetectionBlock:^(CGPoint movement, CGFloat intensity, CMTime time) {
            NSLog(@"movement X:%f Y:%f inte:%f",movement.x,movement.y,intensity);
            if (intensity > 0.15f && movement.x > 0.7f) {
                [weakSelf hitWithVector:movement intensity:intensity];
            }
        }];
        [videoCamera startCameraCapture];
        
        UIDevice *device = [UIDevice currentDevice];
        device.proximityMonitoringEnabled = YES;
        [[NSNotificationCenter defaultCenter]
         addObserver:self selector:@selector(proximityChanged:)
         name:@"UIDeviceProximityStateDidChangeNotification" object:nil];
    }
    return self;
}

- (void) proximityChanged: (NSNotification *)note {
    UIDevice *device = [note object];
    NSLog(@"In proximity: %i", device.proximityState);
}


-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    if (speed > 0) {
        if (!CGRectIntersectsRect(firstBg.frame, self.frame)) {
            // Switch
            id aux = firstBg;
            firstBg = secondBg;
            secondBg = aux;
        }
        firstBg.position = ccp(firstBg.position.x - speed, firstBg.position.y);
        secondBg.position = ccp(firstBg.position.x + firstBg.frame.size.width, firstBg.position.y);
    }
}

-(void)hitWithVector:(CGPoint)movement intensity:(CGFloat)intensity{
    [self.player hit];
}

#pragma mark - Control

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
        if (location.x > cgmidx(self.frame)) {
            // Move
            speed = 5.0f;
        }else{
            [self.player hit];
        }
        

    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    speed = 0;
}

-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    [self touchesEnded:touches withEvent:event];
}


-(void)outputAccelertionData:(CMAcceleration)acceleration
{
//    NSLog(@"X:%f, Y:%f, Z:%f",acceleration.x,acceleration.y,acceleration.z);
}


@end
